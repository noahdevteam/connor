import { NgModule } from '@angular/core';

import { ConnerSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [ConnerSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [ConnerSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class ConnerSharedCommonModule {}
