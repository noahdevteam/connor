import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ConnerSharedModule } from 'app/shared';
import { NewUserComponent } from './newuser.component';
import { newUserRoute } from './newuser.router';

@NgModule({
    imports: [ConnerSharedModule, RouterModule.forChild([newUserRoute])],
    declarations: [NewUserComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExpNewUserModule {}
