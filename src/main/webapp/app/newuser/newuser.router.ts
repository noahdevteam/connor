import { Route } from '@angular/router';
import { NewUserComponent } from './newuser.component';
import { UserRouteAccessService } from 'app/core';

export const newUserRoute: Route = {
    path: 'newuser',
    component: NewUserComponent,
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'Connor Consulting'
    },
    canActivate: [UserRouteAccessService]
};
