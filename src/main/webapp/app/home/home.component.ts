import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { Router } from '@angular/router';

import { LoginModalService, Principal, Account, LoginService } from 'app/core';
import { PowerService } from 'app/loginform';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.css']
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    isNavbarCollapsed: boolean;
    form: any;
    token: any;
    token1: any;
    public project: boolean;
    public team: boolean;

    constructor(
        private loginService: LoginService,
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private powerService: PowerService,
        private router: Router
    ) {
        this.isNavbarCollapsed = true;
    }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.principal.identity().then(account => {
                this.account = account;
            });
        });
    }

    projectView(event) {
        this.powerService.power(this.form).subscribe(data => {
            if (data) {
                console.log(data, 'newdataaccesstoken');
                this.token = data;
            } else {
            }
        });
        this.project = true;
        this.team = false;
    }

    teamView(event) {
        this.powerService.powerteamview(this.form).subscribe(data => {
            if (data) {
                console.log(data, 'newdataaccesstoken');
                this.token1 = data;
            } else {
            }
        });
        this.team = true;
        this.project = false;
    }

    onEmbedded(event) {
        console.log(event);
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    userAuthendication(view?: string) {
        this.principal.identity(true).then(account => {
            console.log(account);
            if (account) {
                switch (view) {
                    case 'businessView': {
                        console.log('aasdadsdsaasdasdsd');
                        window.open(
                            'https://app.powerbi.com/groups/me/reports/06f557bf-66ce-4172-9a1f-a99061f6170e/ReportSectionda718cabd3311d242352',
                            '_blank'
                        );
                        break;
                    }
                    case 'clientView': {
                        console.log('a');
                        window.open(
                            'https://app.powerbi.com/groups/me/reports/71b389c9-4071-413b-8531-df6928c55c5f/ReportSectionea7184f35082582a92ca',
                            '_blank'
                        );
                        break;
                    }
                    case 'teamView': {
                        console.log('a');
                        window.open(
                            'https://app.powerbi.com/groups/me/reports/07680e4a-7a6e-453a-906d-e7119731d347/ReportSection3cce35e007b6252c878b',
                            '_blank'
                        );
                        break;
                    }
                    case 'projectView': {
                        console.log('a');
                        window.open(
                            'https://app.powerbi.com/groups/me/reports/c23f2cd9-13a4-4329-9e68-6140e7ca0fd9/ReportSection',
                            '_blank'
                        );
                        break;
                    }
                }
            }
        });
    }

    teamViewAuthendication(view?: string) {
        this.principal.identity(true).then(account => {
            console.log(account);
            if (account) {
                switch (view) {
                    case 'businessView': {
                        console.log('aasdadsdsaasdasdsd');
                        window.open(
                            'https://app.powerbi.com/groups/fe351b50-22d7-499e-9752-93024aa77d5b/reports/17b4a6d7-2612-4732-8882-41998bcc2606/ReportSectione0cdc25a91ad6f11c729',
                            '_blank'
                        );
                        break;
                    }
                    case 'clientView': {
                        console.log('a');
                        window.open(
                            'https://app.powerbi.com/groups/fe351b50-22d7-499e-9752-93024aa77d5b/reports/9b7b2bf3-5cdb-4108-8cc4-23aa6186ef56/ReportSectione56d2d28ecb56ee97b07',
                            '_blank'
                        );
                        break;
                    }
                    case 'teamView': {
                        console.log('a');
                        window.open(
                            ' https://app.powerbi.com/groups/fe351b50-22d7-499e-9752-93024aa77d5b/reports/ea5e4b10-3d53-49a9-8454-630ec85bc9e9/ReportSectionfd1c7e6e625d7aac4e7c',
                            '_blank'
                        );
                        break;
                    }
                    case 'projectView': {
                        console.log('a');
                        window.open(
                            'https://app.powerbi.com/groups/fe351b50-22d7-499e-9752-93024aa77d5b/reports/3d01be68-9fb8-4f97-8a78-29c5b6de1cdb/ReportSection17e77fde62445d56090d',
                            '_blank'
                        );
                        break;
                    }
                }
            }
        });
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    openNav() {
        document.getElementById('mySidenav').style.width = '250px';
    }

    closeNav() {
        document.getElementById('mySidenav').style.width = '0';
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
        console.log(this.isNavbarCollapsed, 'auth');
    }

    logout() {
        this.collapseNavbar();
        console.log(this.collapseNavbar(), 'new');
        this.loginService.logout();
        this.router.navigate(['/login']);
    }
}
