import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxPowerBiModule } from 'ngx-powerbi';
import { ConnerSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { PowerBIModule } from 'angular2-powerbi';

@NgModule({
    imports: [NgxPowerBiModule, PowerBIModule, ConnerSharedModule, RouterModule.forChild([HOME_ROUTE])],
    declarations: [HomeComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConnerHomeModule {}
