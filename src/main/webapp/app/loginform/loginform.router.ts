import { Route } from '@angular/router';
import { LoginformComponent } from './loginform.component';
import { UserRouteAccessService } from 'app/core';

export const loginformRoute: Route = {
    path: 'login',
    component: LoginformComponent,
    data: {
        authorities: [],
        pageTitle: 'Connor Consulting'
    }
};
