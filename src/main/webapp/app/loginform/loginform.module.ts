import { PowerService } from './powerbi.service';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ConnerSharedModule } from 'app/shared';
import { LoginformComponent } from './loginform.component';
import { loginformRoute } from './loginform.router';

@NgModule({
    imports: [ConnerSharedModule, RouterModule.forChild([loginformRoute])],
    declarations: [LoginformComponent],
    providers: [PowerService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExpLoginFormModule {}
