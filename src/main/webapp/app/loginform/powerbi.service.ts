import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { createRequestOption } from 'app/shared';
import { SERVER_API_URL } from 'app/app.constants';

@Injectable()
export class PowerService {
    constructor(private http: HttpClient) {}

    power(formData: any) {
        return this.http.post(SERVER_API_URL + 'api/powerbi', formData);
    }
    powerteamview(formData: any) {
        return this.http.post(SERVER_API_URL + 'api/powerbi/teamview', formData);
    }
}
