import { Component, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';

import { LoginService } from 'app/core/login/login.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import swal from 'sweetalert2';
import { PowerService } from './powerbi.service';

@Component({
    selector: 'jhi-loginform',
    templateUrl: './loginform.component.html',
    styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements AfterViewInit {
    authenticationError: boolean;
    password: string;
    rememberMe: boolean;
    email: string;
    credentials: any;
    form: any;
    token: any;

    constructor(
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private stateStorageService: StateStorageService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private powerService: PowerService,
        private router: Router
    ) {
        this.credentials = {};
    }

    ngAfterViewInit() {
        setTimeout(() => this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#email'), 'focus', []), 0);
    }

    cancel() {
        this.credentials = {
            email: null,
            password: null,
            rememberMe: true
        };
        this.authenticationError = false;
    }

    login() {
        // const formData = new FormData ();
        // formData.append('grant_type', 'password');
        // formData.append('scope', 'openid');
        // formData.append('resource', 'https://analysis.windows.net/powerbi/api');
        // formData.append('client_id', '51acb765-b2c8-459d-81cc-aad7445482a5');
        // formData.append('username', 'project.view@connor-consulting.com');
        // formData.append('password', 'Mp5p3DAS');

        this.loginService
            .login({
                email: this.email,
                password: this.password,
                rememberMe: this.rememberMe
            })
            .then(() => {
                console.log(123);
                this.authenticationError = false;
                if (this.router.url === '/register' || /^\/activate\//.test(this.router.url) || /^\/reset\//.test(this.router.url)) {
                    this.router.navigate(['']);
                }
                this.eventManager.broadcast({
                    name: 'authenticationSuccess',
                    content: 'Sending Authentication Success'
                });
                swal({
                    title: 'Successfully Logged In',
                    confirmButtonColor: '#022548'
                });
                this.powerService.power(this.form).subscribe(data => {
                    if (data) {
                        this.token = data;
                    } else {
                    }
                });
                // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // // since login is succesful, go to stored previousState and clear previousState
                this.router.navigate(['']);

                const redirect = this.stateStorageService.getUrl();
                if (redirect) {
                    this.stateStorageService.storeUrl(null);
                    this.router.navigate([redirect]);
                }
            })
            .catch(() => {
                this.authenticationError = true;
            });
    }

    register() {
        // this.activeModal.dismiss('to state register');
        this.router.navigate(['/register']);
    }

    requestResetPassword() {
        // this.activeModal.dismiss('to state requestReset');
        this.router.navigate(['/reset', 'request']);
    }
}
