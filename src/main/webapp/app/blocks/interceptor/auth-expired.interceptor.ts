import { Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoginService } from 'app/core/login/login.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

export class AuthExpiredInterceptor implements HttpInterceptor {
    constructor(private injector: Injector, private router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(
                (event: HttpEvent<any>) => {},
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        if (err.status === 401) {
                            const loginService: LoginService = this.injector.get(LoginService);
                            loginService.logout();
                            // console.log('logout');
                            // swal('Your session is expired.Please login');
                            // console.log(err.url);
                            // console.log(err.url.includes('/api/account'));
                            // if (!(err.url && err.url.includes('/api/account'))) {
                            //     swal('Your session is expired.Please login');
                            // }
                        }
                    }
                }
            )
        );
    }
}
