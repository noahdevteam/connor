import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { User, UserService } from 'app/core';
import swal from 'sweetalert2';
import { Location } from '@angular/common';

@Component({
    selector: 'jhi-user-mgmt-update',
    templateUrl: './user-management-update.component.html'
})
export class UserMgmtUpdateComponent implements OnInit {
    user: User;
    languages: any[];
    authorities: any[];
    dashboard: any[];
    isSaving: boolean;
    success: string;
    roleName: any;

    constructor(private userService: UserService, private route: ActivatedRoute, private router: Router, private _location: Location) {}

    ngOnInit() {
        this.isSaving = false;
        this.route.data.subscribe(({ user }) => {
            this.user = user.body ? user.body : user;
        });
        this.authorities = [];
        this.userService.authorities().subscribe(authorities => {
            this.authorities = authorities;
            console.log(this.authorities, '123');
        });
        this.dashboard = [];
        this.userService.dashboard().subscribe(dashboard => {
            this.dashboard = dashboard;
            console.log(this.dashboard);
        });
    }

    cancel() {
        this._location.back();
    }

    fun(data) {
        console.log(data);
        this.roleName = data;
        console.log(this.roleName);
        if (this.roleName === 'Admin') {
            this.user.dashboard = ['Project View', 'Team View', 'Client View', 'Business View'];
        }
        if (this.roleName === 'Executive') {
            this.user.dashboard = ['Project View', 'Team View', 'Client View', 'Business View'];
        }
        if (this.roleName === 'Account Executive') {
            this.user.dashboard = ['Project View', 'Team View', 'Client View', 'Business View'];
        }
        if (this.roleName === 'Account Manager') {
            this.user.dashboard = ['Project View', 'Team View', 'Client View'];
        }
        if (this.roleName === 'Team Member') {
            this.user.dashboard = ['Project View', 'Team View', 'Client View'];
        }
        if (this.roleName === 'Client') {
            this.user.dashboard = ['Project View', 'Client View'];
        }
        if (this.roleName === 'Custom Role') {
            this.user.dashboard = [];
        }
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.user.id !== null) {
            this.userService.update(this.user).subscribe(response => this.onSaveSuccess(response), () => this.onSaveError());
        } else {
            this.user.langKey = 'en';
            this.userService.create(this.user).subscribe(response => this.onSaveSuccess(response), () => this.onSaveError());
        }
    }

    private onSaveSuccess(result) {
        this.isSaving = false;
        this.success = 'OK';
        swal('User Created Successfully');
        this.router.navigate(['']);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
