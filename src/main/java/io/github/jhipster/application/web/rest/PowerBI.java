package io.github.jhipster.application.web.rest;


import com.google.gson.Gson;
import io.github.jhipster.application.web.rest.util.PaginationUtil;
import jdk.nashorn.internal.parser.JSONParser;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.naming.AuthenticationException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PowerBI {

    @PostMapping(value = "/powerbi",produces = "application/json")
    public String PowerBIProjectViewToken()
    {
        String responseAckBody = "";
        JSONObject resultset = new JSONObject();
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost("https://login.microsoftonline.com/common/oauth2/token");
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("grant_type", "password"));
            nvps.add(new BasicNameValuePair("scope", "openid"));
            nvps.add(new BasicNameValuePair("resource", "https://analysis.windows.net/powerbi/api"));
            nvps.add(new BasicNameValuePair("client_id", "51acb765-b2c8-459d-81cc-aad7445482a5"));
            nvps.add(new BasicNameValuePair("username", "project.view@connor-consulting.com"));
            nvps.add(new BasicNameValuePair("password", "Mp5p3DAS"));


            post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));


            //DefaultHttpClient httpClient = new DefaultHttpClient();
            CloseableHttpResponse response = client.execute(post);
            responseAckBody = EntityUtils.toString(response.getEntity());
            System.out.println("sdssssssssssssssss"+responseAckBody);


            resultset = new JSONObject(responseAckBody);
            resultset.accumulate("url","https://app.powerbi.com/reportEmbed?reportId=d8b45f11-ac00-48ae-9770-bf79746bdd6e&groupId=b6d712c0-4ac3-4780-bc78-6c821f5f0e84&autoAuth=true");

        }catch (Exception e) {
        System.out.println(e.getMessage());
    }
        return resultset.toString();

    }


    @PostMapping(value = "/powerbi/teamview",produces = "application/json")
    public String PowerBITeamViewToken()
    {
        String responseAckBody = "";
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost("https://login.microsoftonline.com/common/oauth2/token");
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("grant_type", "password"));
            nvps.add(new BasicNameValuePair("scope", "openid"));
            nvps.add(new BasicNameValuePair("resource", "https://analysis.windows.net/powerbi/api"));
            nvps.add(new BasicNameValuePair("client_id", "1381e5db-c6bb-4777-888c-6b892753ad6c"));
            nvps.add(new BasicNameValuePair("username", "team.view@connor-consulting.com"));
            nvps.add(new BasicNameValuePair("password", "Np3p7SAD"));
            post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
            //DefaultHttpClient httpClient = new DefaultHttpClient();
            CloseableHttpResponse response = client.execute(post);
            responseAckBody = EntityUtils.toString(response.getEntity());
            System.out.println(responseAckBody);

        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return responseAckBody;

    }
}
