package io.github.jhipster.application.web.rest.vm;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.application.domain.DashboardEnity;
import io.github.jhipster.application.repository.DashboardRepository;
import io.github.jhipster.application.service.dto.UserDTO;
import io.github.jhipster.application.web.rest.UserResource;
import io.github.jhipster.application.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")

public class DashboardResource {
    private final Logger log = LoggerFactory.getLogger(DashboardResource.class);
    private final DashboardRepository dashboardRepository;


    public DashboardResource(DashboardRepository dashboardRepository) {
        this.dashboardRepository = dashboardRepository;
    }


    /**
     * GET /dashboard : get all dashborad.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all users
     */

    @GetMapping("/dashboard")
    @Timed
    public ResponseEntity<List<DashboardEnity>> getAllDashboard() {
        final List<DashboardEnity> list = dashboardRepository.findAll();
        // HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(list));
    }
}



