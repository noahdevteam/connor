package io.github.jhipster.application.web.rest;

import io.github.jhipster.application.domain.User;
import io.github.jhipster.application.repository.UserRepository;
/*import io.github.jhipster.application.scheduler.ScheduleTasks;*/
import io.github.jhipster.application.security.jwt.JWTFilter;
import io.github.jhipster.application.security.jwt.TokenProvider;

import io.github.jhipster.application.service.UserService;
import io.github.jhipster.application.web.rest.vm.LoginVM;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import static io.github.jhipster.application.domain.User_.email;
import static io.github.jhipster.application.domain.User_.sessionlogin;
import static io.github.jhipster.application.domain.User_.sessiontimein;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManager authenticationManager;

    private final UserRepository userRepository;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManager authenticationManager,UserRepository userRepository) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
    }

    @PostMapping("/authenticate")
    @Timed
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {



        System.out.println("request---->"+loginVM);
        UsernamePasswordAuthenticationToken authenticationToken =new UsernamePasswordAuthenticationToken(loginVM.getEmail(), loginVM.getPassword());

        Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        User user = userRepository.findByEmail(loginVM.getEmail());
        user.setSessiontimein(Instant.now());
        userRepository.save(user);
        System.out.println(user.getSessiontimein());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }



    @PostMapping("/logoutSession")
    @Timed
    public void authorize(@Valid @RequestParam("toMail") String email) {

        System.out.println("request---->" + email);

        User user = userRepository.findByEmail(email);

        user.setSessionlogin(false);

        userRepository.save(user);

    }



    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }


        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
