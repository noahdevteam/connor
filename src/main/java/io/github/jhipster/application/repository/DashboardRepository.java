package io.github.jhipster.application.repository;

import io.github.jhipster.application.domain.DashboardEnity;
import io.github.jhipster.application.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface DashboardRepository extends JpaRepository<DashboardEnity, String> {
}
